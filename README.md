# Local development with HTTPS

Prerequisite: Add `127.0.0.1	dnk8n.local` to `/etc/hosts`

1. go get github.com/FiloSottile/mkcert
1. mkcert -install
1. sudo mkdir -p ./caddy/artifacts/certs
1. sudo chown -R $(whoami): ./caddy/artifacts/certs
1. cd ./caddy/artifacts/certs
1. mkcert dnk8n.local "*.dnk8n.local"
1. cd -
1. sudo chown -R root: ./caddy/artifacts/certs

You are now in a position to `docker-compose up -d` from the root directory of this project
